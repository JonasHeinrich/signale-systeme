# Aufgabe

## Explizite Lösung

- für u(t) = 1(t)

```matlab
sys = tf([7, 0], [4, 2])
syms x1(t)
X = [x1(t)]
u(t)= heaviside(t)
X0 = [x1(0) == 0]
[x1Sol] = dsolve(diff(X) == ss(sys).A * X + ss(sys).B * u(t), X0)
ySol = ss(sys).C * [x1Sol]
```

## Zustandsraum						x

```matlab
sys = tf([7, 0], [4, 2])
ss(sys)
```

## Übertragungsfunktion				x

```matlab

```

## Übergangsfuntion

- G(s) aufschreiben

```matlab
sys = tf([7, 0], [4, 2])
```

## Gewichtsfunktion

```matlab
sys = tf([7, 0], [4, 2])
impulse(sys)
```

![Gewichtsfunktion](img/gewichtsfunktion.png)

## Bode-Diagramm

```matlab
sys = tf([7, 0], [4, 2])
bode(sys)
```

![Bode](img/bode.png)

## Ortskurve

```matlab
sys = tf([7, 0], [4, 2])
h = nyquistplot(sys)
setoptions(h, 'ShowFullContour', 'off')
```

![Ortskurve](img/nyquist.png)

## Pol-Nullstellenplot

```matlab
sys = tf([7, 0], [4, 2])
pzmap(sys)
```

![Pol-Nullstellenplot](img/pol-nullstellenplot.png)

## Statische Kennlinie 				

```matlab
sys = tf([7, 0], [4, 2])
dcgain(sys)
```